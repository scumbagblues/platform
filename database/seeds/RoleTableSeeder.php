<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete roles table records
        DB::table('roles')->delete();
         
    	$roles = [['name'=>'admin','display_name' => 'admin','description' => 'Super User']];

    	foreach ($roles as $value) {
    		 Role::create($value);
    	}
    }
}
