<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete users table records
        DB::table('users')->delete();
         
    	$users = [['name'=>'admin','email' => 'admin@admin.com','password' => bcrypt('admin')],
                  //['name'=>'admin','email' => 'admin@admin.com','pass' => 'admin'],     
        ];

    	foreach ($users as $value) {
    	   User::create($value);
    	}

        //Only the first user will be have access to all system
        $user = DB::table('users')->first();    
        $role = DB::table('roles')->first();
        $permissions = DB::table('permissions')->limit(4)->get()->toArray();
        
        DB::table('role_user')->insert(['user_id' => $user->id, 'role_id' => $role->id]);

        foreach ($permissions as $key => $permission) {
            DB::table('permission_role')
                ->insert(['permission_id' => $permission->id, 'role_id' => $role->id]);
        }
    }
}
